from flask import Blueprint, request, jsonify
from conection_db.conection import conexion
#till att kunna hitta en bok genom att kalla pa olika

search_blueprint = Blueprint('search', __name__)
_ ,mysql = conexion()

@search_blueprint.route("/books/search", methods=['GET'])
def search_books():
    title = request.args.get('title', '')
    author = request.args.get('author', '')
    genre = request.args.get('genre', '')

    connection = mysql.connection.cursor()
    sql_query = """
        SELECT a.*, b.*
        FROM books AS a
        LEFT JOIN recension AS b ON a.id = b.books_id 
        WHERE a.title LIKE %s 
            AND a.author LIKE %s 
            AND a.genre LIKE %s 
        ORDER BY b.betyg DESC
    """
    title_with_percentages = f"%{title}%"
    author_with_percentages = f"%{author}%"
    genre_with_percentages = f"%{genre}%"

    connection.execute(sql_query, (title_with_percentages, author_with_percentages, genre_with_percentages))
    result = connection.fetchall()
    connection.close()

    return jsonify(result)
