from flask import Blueprint, request, jsonify
from conection_db.conection import conexion


user_blueprint = Blueprint('user', __name__)
_ ,mysql = conexion()

@user_blueprint.route("/user", methods=["GET"])
def get_all_user():
    connections = mysql.connection.cursor()
    connections.execute("""select * from user""")
    result = connections.fetchall()
    return jsonify(result)

@user_blueprint.route("/user/<int:user_id>", methods=['GET'])
def get_user(user_id):
    connection = mysql.connection.cursor()
    connection.execute("""SELECT * FROM user WHERE id = %s""", (user_id,))
    result = connection.fetchone()
    connection.close()
    return jsonify(result)

@user_blueprint.route("/user", methods=['POST'])
def create_user():
    data = request.get_json()
    name = data.get('name')
    last_name = data.get('last_name')
    phone = data.get('phone')
    email = data.get('email')
    password = data.get('password')
    address = data.get('address')
    created_at = data.get('created_at')
    
    connection = mysql.connection.cursor()
    connection.execute("""INSERT INTO user (name, last_name, phone, email, password, address, created_at) VALUES (%s, %s, %s, %s, %s, %s, %s)""", (name, last_name, phone, email, password, address, created_at))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "User created successfully"})

@user_blueprint.route("/user/<int:user_id>", methods=['PUT'])
def update_user(user_id):
    data = request.get_json()
    name = data.get('name')
    last_name = data.get('last_name')
    phone = data.get('phone')
    email = data.get('email')
    password = data.get('password')
    address = data.get('address')
    updated_at = data.get('updated_at')

    connection = mysql.connection.cursor()
    connection.execute("""UPDATE user SET name = %s, last_name = %s, phone = %s, email = %s, password = %s, address = %s, updated_at = %s WHERE id = %s""", (name, last_name, phone, email,password, address, updated_at, user_id))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "User updated successfully"})

@user_blueprint.route("/user/<int:user_id>", methods=['DELETE'])
def delete_user(user_id):
    connection = mysql.connection.cursor()
    connection.execute("""DELETE FROM user WHERE id = %s""", (user_id,))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "User deleted successfully"})

@user_blueprint.route("/user/search/<username>", methods=['GET'])
def search_user_by_username(username):
    connection = mysql.connection.cursor()
    connection.execute("""SELECT * FROM user WHERE name = %s""", (username,))
    result = connection.fetchall()
    connection.close()
    return jsonify(result)

