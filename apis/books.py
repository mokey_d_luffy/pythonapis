from flask import Blueprint, request, jsonify
from conection_db.conection import conexion


books_blueprint = Blueprint('books', __name__)
_ ,mysql = conexion()

@books_blueprint.route("/api/books", methods=["GET"])
def get_all_books():
    connections = mysql.connection.cursor()
    connections.execute("""select * from books""")
    result = connections.fetchall()
    return jsonify(result)

@books_blueprint.route("/api/books/<int:book_id>", methods=['GET'])
def get_book(book_id):
    connection = mysql.connection.cursor()
    connection.execute("""SELECT * FROM books WHERE id = %s""", (book_id,))
    result = connection.fetchone()
    connection.close()
    return jsonify(result)

@books_blueprint.route("/api/books", methods=['POST'])
def create_book():
    data = request.get_json()
    title = data.get('title')
    author = data.get('author')
    description = data.get('description')
    genre = data.get('genre')
    user_id = data.get('user_id')
    created_at = data.get('created_at')
    
    connection = mysql.connection.cursor()
    connection.execute("""INSERT INTO books (title, author, description, genre, user_id, created_at) VALUES (%s, %s, %s, %s, %s, %s)""", (title, author, description, genre, user_id, created_at))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "Books created successfully"})

@books_blueprint.route("/api/book/<int:book_id>", methods=['PUT'])
def update_book(book_id):
    data = request.get_json()
    title = data.get('title')
    author = data.get('author')
    description = data.get('description')
    genre = data.get('genre')
    user_id = data.get('user_id')
    updated_at = data.get('updated_at')

    connection = mysql.connection.cursor()
    connection.execute("""UPDATE books SET title = %s, author = %s, description = %s, genre = %s, user_id = %s, updated_at = %s WHERE id = %s""", (title, author, description, genre,user_id, updated_at, book_id))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "Book updated successfully"})

@books_blueprint.route("/api/book/<int:book_id>", methods=['DELETE'])
def delete_book(book_id):
    connection = mysql.connection.cursor()
    connection.execute("""DELETE FROM books WHERE id = %s""", (book_id,))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "Book deleted successfully"})

@books_blueprint.route("/api/book/search/<title>", methods=['GET'])
def search_book_by_title(title):
    connection = mysql.connection.cursor()
    connection.execute("""SELECT * FROM books WHERE title = %s""", (title,))
    result = connection.fetchall()
    connection.close()
    return jsonify(result)


