from flask import Blueprint, request, jsonify
from conection_db.conection import conexion
#blueprint ger mig bra insikt

betyg_blueprint = Blueprint('betyg', __name__)
_ ,mysql = conexion()

@betyg_blueprint.route("/api/betyg", methods=["GET"])
def get_all_betyg():
    connections = mysql.connection.cursor()
    connections.execute("""select * from recension""")
    result = connections.fetchall()
    #visa alla register
    return jsonify(result)
#denna funktion kallar get
@betyg_blueprint.route("/api/betyg/<int:book_id>", methods=['GET'])
#denna kallar enbart en betyg
def get_getyg(book_id):
    connection = mysql.connection.cursor()
    connection.execute("""SELECT * FROM recension WHERE books_id = %s""", (book_id,))
    result = connection.fetchone()
    connection.close()
    return jsonify(result)

@betyg_blueprint.route("/api/betyg", methods=['POST'])
def create_betyg():
    data = request.get_json()
    recensions = data.get('recensions')
    books_id = data.get('books_id')
    betyg = data.get('betyg')
    created_at = data.get('created_at')
    
    connection = mysql.connection.cursor()
    connection.execute("""INSERT INTO recension (recensions, books_id, betyg, created_at) VALUES (%s, %s, %s, %s)""", (recensions, books_id, betyg, created_at))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "Betyg created successfully"})

@betyg_blueprint.route("/api/betyg/<int:betyg_id>", methods=['PUT'])
def update_betyg(betyg_id):
    data = request.get_json()
    recensions = data.get('recensions')
    books_id = data.get('books_id')
    betyg = data.get('betyg')
    updated_at = data.get('updated_at')

    connection = mysql.connection.cursor()
    connection.execute("""UPDATE recension SET recensions = %s, books_id = %s, betyg = %s, updated_at = %s WHERE id = %s""", (recensions, books_id, betyg,updated_at, betyg_id))
    mysql.connection.commit()
    connection.close()

    return jsonify({"message": "Betyg updated successfully"})


