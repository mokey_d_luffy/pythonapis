import unittest
from flask import Flask
from unittest.mock import patch
from app import app

class TestUserBlueprint(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        
    def tearDown(self):
        self.app_context.pop()

    def test_get_all_user(self):
        response = self.app.get('/user')
        self.assertEqual(response.status_code, 200)

    def test_get_user(self):
        response = self.app.get('/user/1')
        self.assertEqual(response.status_code, 200)

    def test_create_user(self):
        data = {
            "name": "John",
            "last_name": "Doe",
            "phone": "123456789",
            "email": "john@example.com",
            "password": "password123",
            "address": "123 Main St",
            "created_at": "2022-01-01"
        }
        response = self.app.post('/user', json=data)
        self.assertEqual(response.status_code, 200)

    def test_update_user(self):
        data = {
            "name": "Updated Name",
            "last_name": "Updated Last Name",
            "phone": "987654321",
            "email": "updated@example.com",
            "password": "updatedpassword123",
            "address": "456 Updated St",
            "updated_at": "2022-02-01"
        }
        response = self.app.put('/user/1', json=data)
        self.assertEqual(response.status_code, 200)

    def test_delete_user(self):
        response = self.app.delete('/user/1')
        self.assertEqual(response.status_code, 200)

    def test_search_user_by_username(self):
        response = self.app.get('/user/search/Ramiro')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
