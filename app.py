from flask import Flask 
from apis.user import user_blueprint
from apis.books import books_blueprint
from apis.betyg import betyg_blueprint
from apis.search import search_blueprint
from conection_db.conection import conexion

app, _ = conexion()
app.register_blueprint(user_blueprint)
app.register_blueprint(books_blueprint)
app.register_blueprint(betyg_blueprint)
app.register_blueprint(search_blueprint)

if __name__ == "__main__":
    app.run(debug=True)

